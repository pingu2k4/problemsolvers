﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using BrocardsProblem.Commands;
using BrocardsProblem.Models;
using BrocardsProblem.Views;
using System.IO;
using System.Numerics;

namespace BrocardsProblem.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            SettingsUpgrade();
            SetBGOpacity();

            FileLocation = Properties.Settings.Default.LastLocation;

            BGWorker = new BackgroundWorker();
            BGWorker.WorkerReportsProgress = true;
            BGWorker.WorkerSupportsCancellation = true;
            BGWorker.DoWork += BGWorker_DoWork;
            BGWorker.ProgressChanged += BGWorker_ProgressChanged;
            BGWorker.RunWorkerCompleted += BGWorker_RunWorkerCompleted;
        }

        private void BGWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsRunning = false;
            IsStopped = true;
            Status = "Complete.";
        }

        private void BGWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Tuple<string, BigInteger> State = e.UserState as Tuple<string, BigInteger>;

            Status = State.Item1 + " - " + State.Item2.ToString("0,0");
        }

        private void BGWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            if (NewFile)
            {
                using (StreamWriter ResultsWriter = File.CreateText(@FileLocation + "Results.dat"))
                {
                    ResultsWriter.WriteLine("N,N!,M");
                }
            }
            else
            {
                if (!File.Exists(@FileLocation + "Results.dat"))
                {
                    Status = "Results.dat does not exist! Check Create New file to create one.";
                    return;
                }
            }

            BigInteger i = 0;
            BigInteger StartN;
            BigInteger iFactorial = 1;
            BigInteger Updates = 10000;

            BigInteger.TryParse(UpdateFrequency, out Updates);

            if(UseSeeds)
            {
                Status = "Parsing Large Numbers...";
                if(!BigInteger.TryParse(SeedNumberString, out i))
                {
                    Status = "Unable to parse Seed Number.";
                    return;
                }
                if(!BigInteger.TryParse(SeedFactorialString, out iFactorial))
                {
                    Status = "Unable to parse Seed Factorial.";
                    return;
                }
            }

            if(!BigInteger.TryParse(StartingNumberString, out StartN))
            {
                Status = "Unable to parse starting number.";
                return;
            }

            BigInteger Factor100 = BigInteger.One;
            BigInteger Factor10000 = BigInteger.One;
            Status = "Factorising";
            for (i++; i <= StartN; i++)
            {
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                if (i % 10000 == 0)
                {
                    Factor100 = Factor100 * i;
                    Factor10000 = Factor10000 * Factor100;
                    iFactorial = iFactorial * Factor10000;
                    Factor100 = BigInteger.One;
                    Factor10000 = BigInteger.One;
                }
                else if (i % 100 == 0)
                {
                    Factor100 = Factor100 * i;
                    Factor10000 = Factor10000 * Factor100;
                    Factor100 = BigInteger.One;
                }
                else
                {
                    Factor100 = Factor100 * i;
                }

                //iFactorial = i * iFactorial;

                if (i % Updates == 0)
                {
                    Worker.ReportProgress(50, new Tuple<string, BigInteger>("Factorialising", i));
                    using (StreamWriter DropWriter = File.CreateText(@FileLocation + "FactorialDropCatcher.dat"))
                    {
                        DropWriter.WriteLine("N: " + i);
                        DropWriter.WriteLine("N!: " + iFactorial);
                    }
                }
            }

            i = StartN;

            while(true)
            {
                if(Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                BigInteger Square = BigInteger.Zero;
                if (IsPerfectSquareBS(iFactorial + 1, ref Square))
                {
                    using (StreamWriter ResultsWriter = File.AppendText(@FileLocation + "Results.dat"))
                    {
                        ResultsWriter.WriteLine(i + "," + iFactorial + "," + Square);
                    }
                }

                i++;
                iFactorial = i * iFactorial;

                if (i % Updates == 0)
                {
                    Worker.ReportProgress(50, new Tuple<string, BigInteger>("Testing", i));
                    if(i % (Updates * 100) == 0)
                    {
                        using (StreamWriter DropWriter = File.CreateText(@FileLocation + "FactorialDropCatcher.dat"))
                        {
                            DropWriter.WriteLine("N: " + i);
                            DropWriter.WriteLine("N!: " + iFactorial);
                        }
                    }
                }
            }
        }

        private bool IsPerfectSquareBS(BigInteger n, ref BigInteger sqr)
        {
            sqr = -1;
            if (n < 0)
            {
                return false;
            }
            else if (n == 0 || n == 1)
            {
                sqr = n;
                return true;
            }
            //Find the lower and upper limits
            BigInteger lower = 1;
            BigInteger upper = 2;
            while (n < (lower * lower) || n > (upper * upper))
            {
                lower = upper;
                upper *= upper;
            }
            //Binary search
            while (lower + 1 < upper)
            {
                BigInteger mid = (lower + upper) / 2;
                BigInteger midSq = mid * mid;
                if (midSq == n)
                {
                    sqr = mid;
                    return true;
                }
                else if (midSq > n)
                {
                    upper = mid;
                }
                else
                {
                    lower = mid;
                }
            }
            return false;
        }

        #region BOOT
        private void SettingsUpgrade()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }
        #endregion

        #region COMMANDS
        private ICommand start;
        public ICommand Start
        {
            get
            {
                if (start == null)
                {
                    start = new RelayCommand(StartEx, null);
                }
                return start;
            }
        }
        private void StartEx(object p)
        {
            string LastChar = FileLocation.Substring(FileLocation.Length - 1);
            if (LastChar != "/" && LastChar != "\\")
            {
                FileLocation += "\\";
            }

            try
            {
                Directory.CreateDirectory(FileLocation);
            }
            catch
            {
                Status = "Unable to access the given location!";
                return;
            }

            IsStopped = false;
            IsRunning = true;

            BGWorker.RunWorkerAsync();
        }

        private ICommand stop;
        public ICommand Stop
        {
            get
            {
                if (stop == null)
                {
                    stop = new RelayCommand(StopEx, null);
                }
                return stop;
            }
        }
        private void StopEx(object p)
        {
            if (BGWorker.WorkerSupportsCancellation && BGWorker.IsBusy)
            {
                BGWorker.CancelAsync();
            }
        }
        #endregion

        #region PROPERTIES
        #region PANELS

        #endregion

        #region PROPS
        private string startingNumberString = "10000000000";
        public string StartingNumberString
        {
            get
            {
                return startingNumberString;
            }
            set
            {
                startingNumberString = value;
                OnPropertyChanged("StartingNumberString");
            }
        }

        private string seedNumberString;
        public string SeedNumberString
        {
            get
            {
                return seedNumberString;
            }
            set
            {
                seedNumberString = value;
                OnPropertyChanged("SeedNumberString");
            }
        }

        private string seedFactorialString;
        public string SeedFactorialString
        {
            get
            {
                return seedFactorialString;
            }
            set
            {
                seedFactorialString = value;
                OnPropertyChanged("SeedFactorialString");
            }
        }

        private string updateFrequency = "10000";
        public string UpdateFrequency
        {
            get
            {
                return updateFrequency;
            }
            set
            {
                updateFrequency = value;
                OnPropertyChanged("UpdateFrequency");
            }
        }

        private bool useSeeds = false;
        public bool UseSeeds
        {
            get
            {
                return useSeeds;
            }
            set
            {
                useSeeds = value;
                OnPropertyChanged("UseSeeds");
            }
        }

        private string fileLocation;
        public string FileLocation
        {
            get
            {
                return fileLocation;
            }
            set
            {
                fileLocation = value;
                Properties.Settings.Default.LastLocation = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("FileLocation");
            }
        }

        private bool newFile = true;
        public bool NewFile
        {
            get
            {
                return newFile;
            }
            set
            {
                newFile = value;
                OnPropertyChanged("NewFile");
            }
        }

        private string status = "Ready";
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }

        private bool isStopped = true;
        public bool IsStopped
        {
            get
            {
                return isStopped;
            }
            set
            {
                isStopped = value;
                OnPropertyChanged("IsStopped");
            }
        }

        private bool isRunning = false;
        public bool IsRunning
        {
            get
            {
                return isRunning;
            }
            set
            {
                isRunning = value;
                OnPropertyChanged("IsRunning");
            }
        }

        BackgroundWorker BGWorker;
        #endregion

        #region SETTINGS
        public string BGImage
        {
            get
            {
                if (Properties.Settings.Default.BackgroundImage == 0)
                {
                    return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".png";
                }
                return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".jpg";
            }
            set
            {
                OnPropertyChanged("BGImage");
            }
        }

        private double bgOpacity;
        public double BGOpacity
        {
            get
            {
                return bgOpacity;
            }
            set
            {
                bgOpacity = value;
                OnPropertyChanged("BGOpacity");
            }
        }
        private void SetBGOpacity()
        {
            switch (Properties.Settings.Default.BackgroundImage)
            {
                case 1:
                    BGOpacity = 0.1;
                    break;
                case 2:
                    BGOpacity = 0.1;
                    break;
                case 3:
                    BGOpacity = 0.3;
                    break;
                case 4:
                    BGOpacity = 0.6;
                    break;
                case 5:
                    BGOpacity = 0.2;
                    break;
                case 6:
                    BGOpacity = 0.6;
                    break;
                case 7:
                    BGOpacity = 0.2;
                    break;
                case 8:
                    BGOpacity = 0.2;
                    break;
                case 9:
                    BGOpacity = 0.1;
                    break;
                case 10:
                    BGOpacity = 0.5;
                    break;
                default:
                    BGOpacity = 0.5;
                    break;
            }
        }
        #endregion
        #region MISC
        private ProgramVersion programVersion = new ProgramVersion();
        public string ProgramVersion
        {
            get
            {
                return programVersion.Version;
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        #region MISC
        private ICommand openPreferences;
        public ICommand OpenPreferences
        {
            get
            {
                if (openPreferences == null)
                {
                    openPreferences = new RelayCommand(OpenPreferencesEx, null);
                }
                return openPreferences;
            }
        }
        private void OpenPreferencesEx(object p)
        {
            PreferencesWindow PrefWindow = new PreferencesWindow();
            PreferencesWindowViewModel PrefWindowViewModel = new PreferencesWindowViewModel();
            PrefWindow.DataContext = PrefWindowViewModel;
            PrefWindow.Owner = p as MainWindow;
            PrefWindow.ShowDialog();

            if (PrefWindowViewModel.Saved)
            {
                if (PrefWindowViewModel.BG0)
                {
                    Properties.Settings.Default.BackgroundImage = 0;
                }
                if (PrefWindowViewModel.BG1)
                {
                    Properties.Settings.Default.BackgroundImage = 1;
                }
                if (PrefWindowViewModel.BG2)
                {
                    Properties.Settings.Default.BackgroundImage = 2;
                }
                if (PrefWindowViewModel.BG3)
                {
                    Properties.Settings.Default.BackgroundImage = 3;
                }
                if (PrefWindowViewModel.BG4)
                {
                    Properties.Settings.Default.BackgroundImage = 4;
                }
                if (PrefWindowViewModel.BG5)
                {
                    Properties.Settings.Default.BackgroundImage = 5;
                }
                if (PrefWindowViewModel.BG6)
                {
                    Properties.Settings.Default.BackgroundImage = 6;
                }
                if (PrefWindowViewModel.BG7)
                {
                    Properties.Settings.Default.BackgroundImage = 7;
                }
                if (PrefWindowViewModel.BG8)
                {
                    Properties.Settings.Default.BackgroundImage = 8;
                }
                if (PrefWindowViewModel.BG9)
                {
                    Properties.Settings.Default.BackgroundImage = 9;
                }
                if (PrefWindowViewModel.BG10)
                {
                    Properties.Settings.Default.BackgroundImage = 10;
                }
                Properties.Settings.Default.Save();
                OnPropertyChanged("BGImage");
                SetBGOpacity();
            }
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
