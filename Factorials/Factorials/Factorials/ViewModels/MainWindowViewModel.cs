﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using Factorials.Commands;
using Factorials.Models;
using Factorials.Views;
using System.Numerics;
using System.IO;

namespace Factorials.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            SettingsUpgrade();
            SetBGOpacity();

            FileLocation = Properties.Settings.Default.LastLocation;

            BGWorker = new BackgroundWorker();
            BGWorker.WorkerReportsProgress = true;
            BGWorker.WorkerSupportsCancellation = true;
            BGWorker.DoWork += BGWorker_DoWork;
            BGWorker.ProgressChanged += BGWorker_ProgressChanged;
            BGWorker.RunWorkerCompleted += BGWorker_RunWorkerCompleted;
        }

        private void BGWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsRunning = false;
            IsStopped = true;

            Status = "Complete!";
        }

        private void BGWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BGWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int NValue;
            if(!int.TryParse(N, out NValue))
            {
                Status = "Unable to parse a value from N.";
                return;
            }
            BigInteger Result = Factorial(NValue);

            Status = "Writing Number";

            //using (StreamWriter DropWriter = File.CreateText(@FileLocation + "Factorial.dat"))
            //{
            //    DropWriter.WriteLine("N: " + NValue);
            //    DropWriter.WriteLine("N!: " + Result);
            //}
            WriteToFile(@FileLocation + "Factorial.dat", NValue, Result);

            Status = "Complete!";
        }
        /*
        public BigInteger Factorial(int n)
        {
            if (n< 0)
            {
                throw new System.ArgumentOutOfRangeException("n",
                ": n >= 0 required, but was " + n);
            }
     
            return RecFactorial(n);
        }
     
        private BigInteger RecFactorial(int n)
        {
            if (n< 2) return BigInteger.One;
     
            return BigInteger.Pow(RecFactorial(n / 2), 2) * Swing(n);
        }
     
        private static BigInteger Swing(int n)
        {
            int z;
     
            switch (n % 4)
            {
                case 1: z = n / 2 + 1; break;
                case 2: z = 2; break;
                case 3: z = 2 * (n / 2 + 2); break;
                default: z = 1; break;
            }
     
            var b = new BigInteger(z);
            z = 2 * (n - ((n + 1) & 1));
     
            for (int i = 1; i <= n / 4; i++, z -= 4)
            {
                b = (b* z) / i;
            }
     
            return b;
        }
        */

        /* BEST TESTED
        private long den, num, g, h;
        private int i;
     
        public BigInteger Factorial(int n)
        {
            if (n< 0)
            {
                throw new System.ArgumentOutOfRangeException("n",
                ": n >= 0 required, but was " + n);
            }
     
            return RecFactorial(n);
        }
     
        private BigInteger RecFactorial(int n)
        {
            if (n< 2) return BigInteger.One;
     
            return BigInteger.Pow(RecFactorial(n / 2),2) * Swing(n);
        }
     
        private BigInteger Swing(int n)
        {
            bool oddN = (n & 1) == 1;
            bool div = false;
            h = n / 2;
     
            switch ((n / 2) % 4)
            {
                case 0: h = oddN? h + 1 : 1; break;
                case 1: h = oddN? 2 * (h + 2) : 2; break;
                case 2: h = oddN? 2 * (h + 1) * (h + 3) : 2 * (h + 1);
                    div = n > 7; break;
                case 3: h = oddN? 4 * (h + 2) * (h + 4) : 4 * (h + 2);
                    div = n > 7; break;
            }
     
            g = div? n / 4 : 1;
            num = 2 * (n + 3 + (n & 1));
            den = -1;
            i = n / 8;
     
            return Product(i + 1).Numerator;
        }
     
        private Rational Product(int l)
        {
            if (l > 1)
            {
                int m = l / 2;
                return Product(m) * Product(l - m);
            }
     
            if (i-- > 0)
            {
                num -= 8;
                den += 2;
                return new Rational(num* (num - 4), den* (den + 1));
            }
     
            return new Rational(h, g);
        }*/

        private void WriteToFile(string Path, BigInteger N, BigInteger NFactorial)
        {
            string NewFile = "N: " + N + "\nN!: " + NFactorial;
            byte[] Buffer = Encoding.ASCII.GetBytes(NewFile);
            int BufferSize = Buffer.Length;

            using (var File = new FileStream(Path, FileMode.Create))
            {
                var remain = BufferSize;
                var curOffset = 0;

                while (remain > 0)
                {
                    var writeCount = Math.Min(remain, 1024 * 1024);

                    File.Write(Buffer, curOffset, writeCount);
                    remain -= writeCount;
                    curOffset += writeCount;
                }
            }
        }

        private delegate BigInteger ProductDelegate(int from, int to);
     
        public BigInteger Factorial(int n)
        {
            if (n< 0)
            {
                throw new System.ArgumentOutOfRangeException(
                    "n: n >= 0 required, but was " + n);
            }
     
            if (n< 2) return BigInteger.One;
     
            int log2n = FloorLog2(n);
            ProductDelegate prodDelegate = Product;
            var results = new IAsyncResult[log2n];
     
            int high = n, low = n >> 1, shift = low, taskCounter = 0;
     
            // -- It is more efficient to add the big intervals
            // -- first and the small ones later!
            while ((low + 1) < high)
            {
                results[taskCounter++] = prodDelegate.BeginInvoke(low + 1, high, null, null);
                high = low;
                low >>= 1;
                shift += low;
            }

            BigInteger p = BigInteger.One, r = BigInteger.One;
            while (--taskCounter >= 0)
            {
                var R = Task.Factory.StartNew<BigInteger>(() => { return r * p; });
                var t = p * prodDelegate.EndInvoke(results[taskCounter]);
                r = R.Result;
                p = t;
            }
     
            return (r* p) << shift;
        }
     
        private static BigInteger Product(int n, int m)
        {
            n = n | 1;       // Round n up to the next odd number
            m = (m - 1) | 1; // Round m down to the next odd number
     
            if (m == n)
            {
                return new BigInteger(m);
            }
     
            if (m == (n + 2))
            {
                return new BigInteger((long)n* m);
            }
     
            int k = (n + m) >> 1;
            return Product(n, k) * Product(k + 1, m);
        }

        public static int FloorLog2(int n)
        {
            if (n <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(n) + " >= 0 required");
            }
            return BitLength(n) - 1;
        }

        public static int BitLength(int w)
        {
            return (w < 1 << 15
            ? (w < 1 << 7 ? (w < 1 << 3 ? (w < 1 << 1 ? (w < 1 << 0 ? (w < 0 ? 32 : 0) : 1)
            : (w < 1 << 2 ? 2 : 3)) : (w < 1 << 5 ? (w < 1 << 4 ? 4 : 5) : (w < 1 << 6 ? 6 : 7)))
            : (w < 1 << 11 ? (w < 1 << 9 ? (w < 1 << 8 ? 8 : 9) : (w < 1 << 10 ? 10 : 11))
            : (w < 1 << 13 ? (w < 1 << 12 ? 12 : 13) : (w < 1 << 14 ? 14 : 15))))
            : (w < 1 << 23
            ? (w < 1 << 19 ? (w < 1 << 17 ? (w < 1 << 16 ? 16 : 17) : (w < 1 << 18 ? 18 : 19))
            : (w < 1 << 21 ? (w < 1 << 20 ? 20 : 21) : (w < 1 << 22 ? 22 : 23)))
            : (w < 1 << 27
            ? (w < 1 << 25 ? (w < 1 << 24 ? 24 : 25) : (w < 1 << 26 ? 26 : 27))
            : (w < 1 << 29 ? (w < 1 << 28 ? 28 : 29)
            : (w < 1 << 30 ? 30 : 31)))));
        }


        #region BOOT
        private void SettingsUpgrade()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }
        #endregion

        #region ICOMMANDS
        private ICommand start;
        public ICommand Start
        {
            get
            {
                if (start == null)
                {
                    start = new RelayCommand(StartEx, null);
                }
                return start;
            }
        }
        private void StartEx(object p)
        {
            string LastChar = FileLocation.Substring(FileLocation.Length - 1);
            if (LastChar != "/" && LastChar != "\\")
            {
                FileLocation += "\\";
            }

            try
            {
                Directory.CreateDirectory(FileLocation);
            }
            catch
            {
                Status = "Unable to access the given location!";
                return;
            }

            IsStopped = false;
            IsRunning = true;

            Status = "Calculating...";

            BGWorker.RunWorkerAsync();
        }

        private ICommand stop;
        public ICommand Stop
        {
            get
            {
                if (stop == null)
                {
                    stop = new RelayCommand(StopEx, null);
                }
                return stop;
            }
        }
        private void StopEx(object p)
        {

        }
        #endregion

        #region PROPERTIES
        #region PANELS

        #endregion

        #region PROPS
        BackgroundWorker BGWorker;

        private string status;
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }

        private bool isStopped = true;
        public bool IsStopped
        {
            get
            {
                return isStopped;
            }
            set
            {
                isStopped = value;
                OnPropertyChanged("IsStopped");
            }
        }

        private bool isRunning = false;
        public bool IsRunning
        {
            get
            {
                return isRunning;
            }
            set
            {
                isRunning = value;
                OnPropertyChanged("IsRunning");
            }
        }

        private string n;
        public string N
        {
            get
            {
                return n;
            }
            set
            {
                n = value;
                OnPropertyChanged("N");
            }
        }

        private string fileLocation;
        public string FileLocation
        {
            get
            {
                return fileLocation;
            }
            set
            {
                fileLocation = value;
                Properties.Settings.Default.LastLocation = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("FileLocation");
            }
        }
        #endregion

        #region SETTINGS
        public string BGImage
        {
            get
            {
                if (Properties.Settings.Default.BackgroundImage == 0)
                {
                    return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".png";
                }
                return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".jpg";
            }
            set
            {
                OnPropertyChanged("BGImage");
            }
        }

        private double bgOpacity;
        public double BGOpacity
        {
            get
            {
                return bgOpacity;
            }
            set
            {
                bgOpacity = value;
                OnPropertyChanged("BGOpacity");
            }
        }
        private void SetBGOpacity()
        {
            switch (Properties.Settings.Default.BackgroundImage)
            {
                case 1:
                    BGOpacity = 0.1;
                    break;
                case 2:
                    BGOpacity = 0.1;
                    break;
                case 3:
                    BGOpacity = 0.3;
                    break;
                case 4:
                    BGOpacity = 0.6;
                    break;
                case 5:
                    BGOpacity = 0.2;
                    break;
                case 6:
                    BGOpacity = 0.6;
                    break;
                case 7:
                    BGOpacity = 0.2;
                    break;
                case 8:
                    BGOpacity = 0.2;
                    break;
                case 9:
                    BGOpacity = 0.1;
                    break;
                case 10:
                    BGOpacity = 0.5;
                    break;
                default:
                    BGOpacity = 0.5;
                    break;
            }
        }
        #endregion
        #region MISC
        private ProgramVersion programVersion = new ProgramVersion();
        public string ProgramVersion
        {
            get
            {
                return programVersion.Version;
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        #region MISC
        private ICommand openPreferences;
        public ICommand OpenPreferences
        {
            get
            {
                if (openPreferences == null)
                {
                    openPreferences = new RelayCommand(OpenPreferencesEx, null);
                }
                return openPreferences;
            }
        }
        private void OpenPreferencesEx(object p)
        {
            PreferencesWindow PrefWindow = new PreferencesWindow();
            PreferencesWindowViewModel PrefWindowViewModel = new PreferencesWindowViewModel();
            PrefWindow.DataContext = PrefWindowViewModel;
            PrefWindow.Owner = p as MainWindow;
            PrefWindow.ShowDialog();

            if (PrefWindowViewModel.Saved)
            {
                if (PrefWindowViewModel.BG0)
                {
                    Properties.Settings.Default.BackgroundImage = 0;
                }
                if (PrefWindowViewModel.BG1)
                {
                    Properties.Settings.Default.BackgroundImage = 1;
                }
                if (PrefWindowViewModel.BG2)
                {
                    Properties.Settings.Default.BackgroundImage = 2;
                }
                if (PrefWindowViewModel.BG3)
                {
                    Properties.Settings.Default.BackgroundImage = 3;
                }
                if (PrefWindowViewModel.BG4)
                {
                    Properties.Settings.Default.BackgroundImage = 4;
                }
                if (PrefWindowViewModel.BG5)
                {
                    Properties.Settings.Default.BackgroundImage = 5;
                }
                if (PrefWindowViewModel.BG6)
                {
                    Properties.Settings.Default.BackgroundImage = 6;
                }
                if (PrefWindowViewModel.BG7)
                {
                    Properties.Settings.Default.BackgroundImage = 7;
                }
                if (PrefWindowViewModel.BG8)
                {
                    Properties.Settings.Default.BackgroundImage = 8;
                }
                if (PrefWindowViewModel.BG9)
                {
                    Properties.Settings.Default.BackgroundImage = 9;
                }
                if (PrefWindowViewModel.BG10)
                {
                    Properties.Settings.Default.BackgroundImage = 10;
                }
                Properties.Settings.Default.Save();
                OnPropertyChanged("BGImage");
                SetBGOpacity();
            }
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }

    class Rational
    { 
        private BigInteger num; // Numerator
        private BigInteger den; // Denominator
    
        public BigInteger Numerator
        {
            get
            {
                BigInteger cd = BigInteger.GreatestCommonDivisor(num, den);
                return num / cd;
            }
        }
       
        public Rational(long _num, long _den)
        {
            long g = Gcd(_num, _den);
            num = new BigInteger(_num / g);
            den = new BigInteger(_den / g);
        }
    
        public Rational(BigInteger _num, BigInteger _den)
        {
            //  If (and only if) the arithmetic supports a
            //  *real* fast Gcd this would lead to a speed up:
            num = _num;
            den = _den;
        }
    
        public static Rational operator *(Rational a, Rational r)
        {
            return new Rational(a.num* r.num, a.den* r.den);
        }
    
        private static long Gcd(long a, long b)
        {
            long x, y;
    
            if (a >= b) { x = a; y = b; }
            else { x = b; y = a; }
    
            while (y != 0)
            {
                long t = x % y; x = y; y = t;
            }
            return x;
        }
    } // endOfRational
}